verbiste (0.1.49-1) unstable; urgency=medium

  * New upstream version 0.1.49

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 10 Aug 2024 12:20:04 +0200

verbiste (0.1.48-1) unstable; urgency=medium

  * New upstream version 0.1.48

 -- Tomasz Buchert <tomasz@debian.org>  Tue, 05 Sep 2023 10:15:01 +0200

verbiste (0.1.47-2) unstable; urgency=medium

  * Remove the verbiste-el package (Closes: ##1032798).

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 08 Jul 2023 12:02:27 +0200

verbiste (0.1.47-1) unstable; urgency=medium

  * New upstream version 0.1.47
  * Update std-ver to 4.4.1 (no changes needed)

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 12 Oct 2019 16:37:15 +0200

verbiste (0.1.46-1) unstable; urgency=medium

  * New upstream version 0.1.46

 -- Tomasz Buchert <tomasz@debian.org>  Mon, 05 Aug 2019 23:34:13 +0200

verbiste (0.1.45-5) unstable; urgency=medium

  * refresh packaging (debhelper 12, std-ver 4.3.0)

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 26 Jan 2019 12:54:29 +0100

verbiste (0.1.45-4) unstable; urgency=medium

  [ Nicholas D Steeves ]
  * Package verbiste-el using dh-elpa. (Closes: #905290)
    - d/control: Add build-depends on dh-elpa.
    - d/rules: Build --with elpa.
    - d/verbiste.el: Declare version 0.7, because the Debian version should
      not be used as the ELPA version; 0.7 was arbitrarily chosen.
    - d/control: Create new package elpa-verbiste, for the Emacs addon.
    - d/control: verbiste-el is now a dummy transitional package.
    - debian/debian-autoloads.el: Copy autoloads from
      verbiste-el.emacsen-startup to the conventional location used
      in dh-elpa packages, because autoloads are not generated without this.
    - Drop obsolete packaging directives: verbiste-el.emacsen-compat,
      verbiste-el.emacsen-install, verbiste-el.emacsen-remove,
      verbiste-el.emacsen-startup, and verbiste-el.install. The files
      elpa-verbiste.elpa and debian-autoloads.el replace these.
  * d/control: Relax debhelper build dep to 11~ to allow backporting.

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 25 Aug 2018 13:32:59 +0200

verbiste (0.1.45-3) unstable; urgency=medium

  * Upload to unstable

 -- Tomasz Buchert <tomasz@debian.org>  Wed, 15 Aug 2018 09:41:03 +0200

verbiste (0.1.45-2) experimental; urgency=medium

  * d/control: update vcs links to salsa
  * Switch to dh compat 11
  * Fix insecure-copyright-format-uri
  * Introduce verbiste-gtk package (make verbiste-gnome transitional)
  * Drop libgnomeui dependency (Closes: #868409)

 -- Tomasz Buchert <tomasz@debian.org>  Tue, 07 Aug 2018 22:36:41 +0200

verbiste (0.1.45-1) unstable; urgency=medium

  * d/control: bumped std-ver to 4.0.0
  * d/control: use debhelper 10
  * d/watch: use watch version 4
  * d/control: drop obsolete dh-autoreconf
  * d/control: add explicit dependency on libxml2
  * New upstream version 0.1.45

 -- Tomasz Buchert <tomasz@debian.org>  Fri, 13 Oct 2017 21:21:59 +0200

verbiste (0.1.44-1) unstable; urgency=medium

  * Remove verbiste-mate-applet (patch Sébastien Villemot)
  * New upstream version 0.1.44

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 27 Nov 2016 16:59:49 +0100

verbiste (0.1.43-1) unstable; urgency=medium

  * Imported Upstream version 0.1.43

 -- Tomasz Buchert <tomasz@debian.org>  Fri, 25 Mar 2016 06:52:02 +0100

verbiste (0.1.42-3) unstable; urgency=medium

  [ Raúl Benencia ]
  * Fix FTBFS due to failing test (Closes: #815742)

  [ Tomasz Buchert ]
  * d/control: bumped std-ver to 3.9.7 (no changes needed)

 -- Tomasz Buchert <tomasz@debian.org>  Thu, 03 Mar 2016 10:38:03 +0100

verbiste (0.1.42-2) unstable; urgency=medium

  * Fix missing dependency (Closes: #803682)
  * Drop menu file

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 01 Nov 2015 20:11:51 +0100

verbiste (0.1.42-1) unstable; urgency=medium

  * Imported Upstream version 0.1.42
  * d/patches: drop patches merged upstream

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 13 Sep 2015 18:49:51 +0200

verbiste (0.1.41-6) unstable; urgency=medium

  * Upload to unstable
  * Make g++ ABI transition (Closes: #791329)
  * Tighten build dependencies

 -- Tomasz Buchert <tomasz@debian.org>  Tue, 04 Aug 2015 13:02:27 +0200

verbiste (0.1.41-5) experimental; urgency=medium

  * Upload wrt G++ 5 ABI transition
  * Upload to experimental

 -- Tomasz Buchert <tomasz@debian.org>  Mon, 03 Aug 2015 16:37:49 +0200

verbiste (0.1.41-4) unstable; urgency=medium

  * Drop symbols file (Closes: #780599)

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 19 Apr 2015 10:39:46 +0200

verbiste (0.1.41-3) unstable; urgency=medium

  [ Kevin Ryde ]
  * Autoload verbiste-el functions (Closes: #714564)
  * Fix verbiste-mode behavior (Closes: #714565)

  [ Tomasz Buchert ]
  * Bumped standards version to 3.9.6 (no changes needed)

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 14 Mar 2015 09:31:29 +0100

verbiste (0.1.41-2) unstable; urgency=medium

  * d/rules: don't override CPPFLAGS, just append
  * Add symbols file for libverbiste
  * Add description for "hardening-no-fortify-functions" override
  * Fix lintian warning "desktop-entry-lacks-keywords-entry" (thanks Tarnyko)
  * Update to newer emacsen policy
  * Add the MATE Applet package (thanks Tarnyko)
  * Add a dependency to libmate-panel-applet

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Tue, 15 Jul 2014 12:25:40 +0200

verbiste (0.1.41-1) unstable; urgency=medium

  * Imported Upstream version 0.1.41
  * Bumped standards version to 3.9.5
  * Added custom gbp settings

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Mon, 07 Apr 2014 11:18:52 +0200

verbiste (0.1.40-1) unstable; urgency=low

  * Imported Upstream version 0.1.40
  * Remove rsvg from build dependencies
  * Added dependency on automake1.11

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Sat, 02 Nov 2013 18:30:14 +0100

verbiste (0.1.39-1) unstable; urgency=low

  * Imported Upstream version 0.1.39 (Closes: #716651)
  * Updated build process (Closes: #727519)
  * Added 3 lintian overrides
  * Add --parallel to dh
  * Fixed URLS in debian/control

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Tue, 29 Oct 2013 09:40:18 +0100

verbiste (0.1.38-1) unstable; urgency=low

  * Imported Upstream version 0.1.38

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Sat, 29 Jun 2013 00:27:16 +0200

verbiste (0.1.37-1) unstable; urgency=low

  * Imported Upstream version 0.1.37

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Tue, 21 May 2013 01:01:23 +0200

verbiste (0.1.36-1) unstable; urgency=low

  * Drop 'DM-Upload-Allowed' field
  * Bumped Standards-Version to 3.9.4
  * Imported Upstream version 0.1.36

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Sun, 05 May 2013 11:58:03 +0200

verbiste (0.1.35-1) experimental; urgency=low

  * Imported Upstream version 0.1.35

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Fri, 23 Nov 2012 18:46:35 +0100

verbiste (0.1.34-2) experimental; urgency=low

  * Link to the correct license
  * Update debhelper compatibility level to 9
  * Port to multi-arch
  * Fix upstream clean script
  * Add 'DM-Upload-Allowed: yes' to the control file

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Fri, 18 May 2012 14:53:58 +0200

verbiste (0.1.34-1) unstable; urgency=low

  * Imported Upstream version 0.1.34

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Fri, 11 May 2012 11:39:36 +0200

verbiste (0.1.33-6) unstable; urgency=low

  * Bump Standards-Version to 3.9.3
  * Rewrite debian/changelog to be machine-readable
  * Symlink .el scripts (closes: #669311)
  * Rework emacsen-startup (closes: #669310)

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Thu, 20 Apr 2012 12:50:00 +0100

verbiste (0.1.33-5) unstable; urgency=low

  * Fixes build problems (closes: #659274)

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Thu, 9 Feb 2012 14:50:32 +0100

verbiste (0.1.33-4) unstable; urgency=low

  * Change maintainer (closes: #640454)
  * Switch build process from cdbs to dh

 -- Tomasz Buchert <tomasz.buchert@inria.fr>  Wed, 8 Feb 2012 11:41:32 +0100

verbiste (0.1.33-3) unstable; urgency=low

  * Disable GNOME applet and remove build dependency (closes: #638103)

 -- Francois Marier <francois@debian.org>  Sun, 11 Sep 2011 17:32:27 +1200

verbiste (0.1.33-2) unstable; urgency=low

  * Remove unneeded .la file from libverbiste-dev (closes: #633241)

  * Bump Standards-Version up to 3.9.2
  * Remove article from package synopsis (lintian warning)

 -- Francois Marier <francois@debian.org>  Mon, 11 Jul 2011 22:43:55 +1200

verbiste (0.1.33-1) unstable; urgency=low

  * New upstream release
  * Remove unnecessary versioned dependencies
  * Bump debhelper compatibility to 8

 -- Francois Marier <francois@debian.org>  Mon, 04 Apr 2011 14:02:31 +1200

verbiste (0.1.32-1) unstable; urgency=low

  * New upstream release
    - includes Danish translation (closes: #607223)
  * Install the missing latex class file (LP: #644679)
  * Bump Standards-Version to 3.9.1

 -- Francois Marier <francois@debian.org>  Wed, 22 Dec 2010 12:09:24 +1300

verbiste (0.1.30-1) unstable; urgency=low

  * New upstream release:
    - add support for Italian verbs

  * Change package description to note the presence of Italian verbs
  * Bump Standards-Version to 3.9.0

 -- Francois Marier <francois@debian.org>  Mon, 05 Jul 2010 12:51:09 +1200

verbiste (0.1.29-1) unstable; urgency=low

  * New upstream release:
    - drop german patch (applied upstream)
  * Fix typos in package description (closes: #576578)

 -- Francois Marier <francois@debian.org>  Sat, 24 Apr 2010 20:46:41 +1200

verbiste (0.1.28-3) unstable; urgency=low

  * Fixes for the German translation (closes: #553915)

 -- Francois Marier <francois@debian.org>  Mon, 01 Feb 2010 17:21:13 +1300

verbiste (0.1.28-2) unstable; urgency=low

  * Bump Standards-Version to 3.8.4
  * Switch to 3.0 (quilt) source format
  * Bump the recommended version of emacs to 23

 -- Francois Marier <francois@debian.org>  Sun, 31 Jan 2010 21:31:05 +1300

verbiste (0.1.28-1) unstable; urgency=low

  * New upstream release (closes: #550198)
  * Update upstream email address

 -- Francois Marier <francois@debian.org>  Sat, 31 Oct 2009 16:31:43 +1300

verbiste (0.1.27-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.3 (no changes)

 -- Francois Marier <francois@debian.org>  Mon, 05 Oct 2009 10:54:50 +1300

verbiste (0.1.26-1) unstable; urgency=low

  * New upstream release:
    - ability to save conjugation tables in LaTeX format
    - new verb: quérir
  * Bump Standards-Version to 3.8.2

 -- Francois Marier <francois@debian.org>  Sun, 05 Jul 2009 12:20:11 +1200

verbiste (0.1.25-2) unstable; urgency=low

  * Fix sections
  * Add dependency on libxml-parser-perl

 -- Francois Marier <francois@debian.org>  Tue, 24 Mar 2009 21:40:11 +1300

verbiste (0.1.25-1) unstable; urgency=low

  * New upstream version
  * Change section to lisp
  * Bump Standards-Version to 3.8.1 (no changes)
  * Bump debhelper compatibility to 7

 -- Francois Marier <francois@debian.org>  Tue, 24 Mar 2009 19:42:44 +1300

verbiste (0.1.24-1) unstable; urgency=low

  * New upstream version
  * Update verbiste.el from upstream git (no code changes)
  * debian/copyright: new URL for verbiste-el
  * Bump Standards-Version to 3.8.0 (no changes)

 -- Francois Marier <francois@debian.org>  Mon, 09 Feb 2009 13:14:29 +1300

verbiste (0.1.23-1) unstable; urgency=low

  * New upstream release
  * Update verbiste.el from upstream darcs (no code changes)
    (closes: #469975)
  * debian/control: use the git protocol instead of http in vcs-git

 -- Francois Marier <francois@debian.org>  Wed, 04 Jun 2008 11:30:41 +1200

verbiste (0.1.22-2) unstable; urgency=low

  * Set the codepage to utf-8 in verbiste.el (Thanks to Nick Steeves!)

 -- Francois Marier <francois@debian.org>  Mon, 25 Feb 2008 22:47:28 +1300

verbiste (0.1.22-1) unstable; urgency=low

  * New upstream version (closes: #449017, #288841, #455303, #329847)
  * Add an emacs extension by Ben Voui to a new "verbiste-el" package

  Packaging changes:
  * Remove Ubuntu patch which was applied upstream
  * Rename library and devel packages to match soname
  * Put the library and the devel package in the right sections
  * Add a different short description for each binary package
  * Bump debhelper compatibility to 6

 -- Francois Marier <francois@debian.org>  Thu, 14 Feb 2008 21:25:52 +1300

verbiste (0.1.19-2) unstable; urgency=low

  * Add a menu entry for verbiste-gnome (closes: #459410)
  * Fix the section name for verbiste-gnome.desktop (closes: #459409)

 -- Francois Marier <francois@debian.org>  Thu, 24 Jan 2008 14:06:05 +1300

verbiste (0.1.19-1) unstable; urgency=low

  * Set myself as maintainer with Sebastien's permission
  * Acknowledging my NMU (closes: #351343)
  * Sync with Ubuntu (includes more recent upstream release)
  * Add to the git collab-maint (mention in debian/control)

  Updating the packaging:
  * Bump debhelper compatibility to 5
  * Add homepage field
  * Add a watch file
  * Remove GPL version and add copyright year in debian/copyright
  * Add HACKING and LISEZMOI to debian/docs
  * Make NEWS the changelog since ChangeLog is not so useful

  Lintian fixes:
  * Bump Standards-Version up to 3.7.3
  * There are no more .a files
  * Use source:Version instead of Source-Version in the dependencies
  * Remove Encoding key from verbiste-gnome.desktop
  * Don't ship the TODO file: it's empty
  * Remove usr/sbin from debian/dirs since we're not installing anything
    there
  * Fix spelling of GNOME in the verbiste-gnome description
  * The dev package version must match the library (binary:Version)
  * Include autotools.mk CDBS file
  * Remove files that upstream installs in /usr/share/doc/
  * Fix minus signs pretending to be hyphens in manpages
  * Install manpages in the right packages
  * Remove rpath

 -- Francois Marier <francois@debian.org>  Wed, 23 Jan 2008 21:26:18 +1300

verbiste (0.1.19-0ubuntu1) feisty; urgency=low

  * New upstream version
  * debian/patches/make-quoting.patch:
    - dropped, not required with the new version
  * debian/rules:
    - build with --with-gtk-app --with-gnome-app --with-gnome-applet
      rather than --with-gnome

 -- Sebastien Bacher <seb128@canonical.com>  Tue,  6 Feb 2007 21:10:08 +0100

verbiste (0.1.14-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * re-run gettextize and autoreconf the original source to avoid overwriting
    /usr/share/locale/locale.alias on amd64 (closes: #351343)
  * Update the Standards-Version (no changes)
  * Remove redundant dependencies (lintian warning)

 -- Francois Marier <francois@debian.org>  Thu,  5 Oct 2006 17:43:07 -0400

verbiste (0.1.14-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/patches/make-quoting.patch: Fix quoting for new make.
    Closes: #344742.

 -- Matej Vela <vela@debian.org>  Wed,  1 Feb 2006 22:08:30 +0100

verbiste (0.1.14-1) unstable; urgency=low

  * New upstream versions:
    - Fixed error in subjunctive present tense of "s:avoir"
    - conjugation template.
    - Fixed error in present conditional tense of "men:tir"
    - conjugation template.
    - Added verbs dissoudre, autodissoudre (Closes: #329846).
    - Fixed error in imperfect tense of "men:tir" conjugation template.
  * debian/control, debian/libverbiste0c2a.install:
    - patch by Daniel T Chen <crimsun@fungus.sh.nu>, rename libverbiste0
      to libverbiste0c2a, and add Replaces/Conflicts (Closes: #342365)

 -- Sebastien Bacher <seb128@debian.org>  Mon, 19 Dec 2005 00:12:40 +0100

verbiste (0.1.11-1) unstable; urgency=low

  * New upstream version (Closes: #316008):
    - fix "inclure" verb (Closes: #296793).

 -- Sebastien Bacher <seb128@debian.org>  Tue, 28 Jun 2005 23:32:09 +0200

verbiste (0.1.10-2) experimental; urgency=low

  * debian/patches/panelfocus.patch:
    - fix the focus issue with the GNOME 2.10.

 -- Sebastien Bacher <seb128@debian.org>  Thu, 14 Apr 2005 15:37:32 +0200

verbiste (0.1.10-1) unstable; urgency=low

  * New upstream release

 -- Sebastien Bacher <seb128@debian.org>  Wed, 30 Mar 2005 15:35:06 +0200

verbiste (0.1.9-2) unstable; urgency=low

  * Rebuilt with gnutls11 (Closes: #264734).

 -- Sebastien Bacher <seb128@debian.org>  Tue, 10 Aug 2004 13:30:28 +0200

verbiste (0.1.9-1) unstable; urgency=low

  * New upstream release.

 -- Sebastien Bacher <seb128@debian.org>  Thu, 29 Apr 2004 23:25:13 +0200

verbiste (0.1.7-2) unstable; urgency=low

  * Specify the depends on libverbiste0 since the new cdbs doesn't handle
    that correctly (Closes:  #226453).

 -- Sebastien Bacher <seb128@debian.org>  Sat, 10 Jan 2004 01:16:54 +0100

verbiste (0.1.7-1) unstable; urgency=low

  * Initial Release.

 -- Sebastien Bacher <seb128@debian.org>  Fri, 14 Nov 2003 23:37:19 +0100
